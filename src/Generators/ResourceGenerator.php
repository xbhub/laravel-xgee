<?php

namespace Xbhub\XGee\Generators;

use Xbhub\XGee\Generators\Migrations\SchemaParser;

/**
 * Class ModelGenerator
 * @package Xbhub\XGee\Generators
 */
class ResourceGenerator extends Generator
{

    /**
     * Get stub name.
     *
     * @var string
     */
    protected $stub = 'resource';

    /**
     * Get root namespace.
     *
     * @return string
     */
    public function getRootNamespace()
    {
        return str_replace('/', '\\', parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode()));
    }

    /**
     * Get generator path config node.
     *
     * @return string
     */
    public function getPathConfigNode()
    {
        return 'resource';
    }

    /**
     * Get destination path for generated file.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode(), true) . '/' . $this->getRequestName() . '.php';
    }

    /**
     * @return string
     */
    protected function getRequestName()
    {
        return ucfirst(trim($this->getClass())).'Resource';
    }


    /**
     * Get array replacements.
     *
     * @return array
     */
    public function getReplacements()
    {
        return array_merge(parent::getReplacements(), [
            'model' => ucfirst(trim($this->getClass())),
            'module' => $this->module
        ]);
    }
}
