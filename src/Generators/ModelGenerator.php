<?php

namespace Xbhub\XGee\Generators;

use Xbhub\XGee\Parser\SchemaParser;


/**
 * Class ModelGenerator
 *
 * @package Xbhub\XGee\Generators
 */
class ModelGenerator extends Generator
{

    /**
     * Get stub name.
     *
     * @var string
     */
    protected $stub = 'model';

    /**
     * Get root namespace.
     *
     * @return string
     */
    public function getRootNamespace()
    {
        return parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode());
    }

    /**
     * Get generator path config node.
     *
     * @return string
     */
    public function getPathConfigNode()
    {
        return 'models';
    }

    /**
     * Get destination path for generated file.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode(), true) . '/' . $this->getName() . '.php';
    }

    /**
     * Get array replacements.
     *
     * @return array
     */
    public function getReplacements()
    {
        return array_merge(parent::getReplacements(), [
            'fillable' => $this->getFillable()
        ]);
    }

    /**
     * Get the fillable attributes.
     *
     * @return string
     */
    public function getFillable()
    {
        if (!$this->fillable) {
            return '[]';
        }
        $results = '[' . PHP_EOL;
        foreach ($this->getSchemaParser()->toArray() as $column => $value) {
            $results .= "\t\t'{$column}'," . PHP_EOL;
        }

        return $results . "\t" . ']';
    }

    /**
     * Get schema parser.
     *
     * @return SchemaParser
     */
    public function getSchemaParser()
    {
        return new SchemaParser($this->fillable);
    }

    public function getModelObject()
    {
        try {
            $segments = $this->getSegments();
            array_pop($segments);
            $rootNamespace = $this->getRootNamespace();
            if ($rootNamespace == false) {
                return null;
            }

            $segments_path = count($segments) > 0 ? implode($segments, '\\') : '';
            $modelName     = rtrim($rootNamespace . '\\' . $segments_path, '\\') . '\\' . $this->getName();
            return new $modelName;
        } catch (\Exception $e) {
            throw new \Exception('model not exists');
        }
    }
}
