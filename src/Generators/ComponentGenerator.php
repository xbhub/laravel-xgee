<?php

namespace Xbhub\XGee\Generators;

use Xbhub\XGee\Generators\Migrations\SchemaParser;

/**
 * Class ModelGenerator
 * @package Xbhub\XGee\Generators
 */
class ComponentGenerator extends Generator
{

    /**
     * Get stub name.
     *
     * @var string
     */
    protected $stub = 'component';

    /**
     * Get root namespace.
     *
     * @return string
     */
    public function getRootNamespace()
    {
        return parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode());
    }

    /**
     * Get generator path config node.
     *
     * @return string
     */
    public function getPathConfigNode()
    {
        return 'component';
    }

    /**
     * Get destination path for generated file.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->getBasePath() .'/'. parent::getConfigGeneratorClassPath($this->getPathConfigNode(), true) . '/' . $this->getRequestName() . '.php';
    }

    /**
     * @return string
     */
    protected function getRequestName()
    {
        return ucfirst(trim($this->getClass()));
    }

    /**
     * Get array replacements.
     *
     * @return array
     */
    public function getReplacements()
    {
        return array_merge(parent::getReplacements(), [
            'fillable' => $this->getFillable(),
            'model' => ucfirst(trim($this->getClass())),
            'modelname' => '$'.mb_strtolower(trim($this->getClass())),
            'modelnamespace' => 'Modules\\'.ucfirst($this->module).'\\Models\\'
        ]);
    }

    /**
     * Get the fillable attributes.
     *
     * @return string
     */
    public function getFillable()
    {
        if (!$this->fillable) {
            return '[]';
        }
        $results = '[' . PHP_EOL;

        foreach ($this->getSchemaParser()->toArray() as $column => $value) {
            $results .= "\t\t'{$column}'," . PHP_EOL;
        }

        return $results . "\t" . ']';
    }

    /**
     * Get schema parser.
     *
     * @return SchemaParser
     */
    public function getSchemaParser()
    {
        return new SchemaParser($this->fillable);
    }
}
