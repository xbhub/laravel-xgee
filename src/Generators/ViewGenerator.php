<?php

namespace Xbhub\XGee\Generators;

use Xbhub\XGee\Generators\Migrations\SchemaParser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * Class ModelGenerator
 * @package Xbhub\XGee\Generators
 */
class ViewGenerator extends Generator
{

    /**
     * Get stub name.
     *
     * @var string
     */
    protected $stub = 'view/index';
    protected $action = '';

    protected $moduleName = '';

    public function __construct(array $options = [])
    {
        parent::__construct($options);

        if($action = $this->getOption('action')) {
            $this->action = $action;
        }
        $this->moduleName = $this->module->getName();
    }

    /**
     * Get root namespace.
     *
     * @return string
     */
    public function getRootNamespace()
    {
        return parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode());
    }

    /**
     * Get generator path config node.
     *
     * @return string
     */
    public function getPathConfigNode()
    {
        return 'view';
    }


    /**
     * Get destination path for generated file.
     *
     * @return string
     */
    public function getPath()
    {
        $resourcePathName = $this->module?'Resources':'resources';
        return $this->getBasePath() . '/' . $resourcePathName . '/views/' .$this->getSubPath(). $this->getPluralName().'/'.$this->action . '.blade.php';
    }

    public function getSubPath()
    {
        $_subpath = $this->subpath;
        return $_subpath?strtolower($_subpath).'/':'';
    }

    /**
     * Gets plural name based on model
     *
     * @return string
     */
    public function getPluralName()
    {
        return lcfirst(ucwords($this->getClass()));
    }

    /**
     * Get array replacements.
     *
     * @return array
     */
    public function getReplacements()
    {
        return array_merge(parent::getReplacements(), [
            'fillable' => $this->getFillable()
        ]);
    }

    /**
     * Get the fillable attributes.
     *
     * @return string
     */
    public function getFillable()
    {
        if (!$this->fillable) {
            return '[]';
        }
        $results = '[' . PHP_EOL;

        foreach ($this->getSchemaParser()->toArray() as $column => $value) {
            $results .= "\t\t'{$column}'," . PHP_EOL;
        }

        return $results . "\t" . ']';
    }

    /**
     * Get schema parser.
     *
     * @return SchemaParser
     */
    public function getSchemaParser()
    {
        return new SchemaParser($this->fillable);
    }

    public function getStub()
    {
        $modelGenerator = new ModelGenerator([
            'name' => $this->getName(),
            'module' => $this->option('module')
        ]);

        $modelObj = $modelGenerator->getModelObject();
        $columns = DB::select( DB::raw('SHOW COLUMNS FROM `'.$modelObj->getTable().'`'));
        $fields = [];
        foreach($columns as $k=>$column) {
            $fields[$k]['name'] = $column->Field;
            $fields[$k]['type'] = $column->Type;
            $fields[$k]['default'] = $column->Default;
        }
        // ['create', 'edit', 'fields', 'index', 'show', 'table'];
        $action = $this->action;
        $replacements = [];
        switch ($action) {
            case 'create':
                $file = 'create';
                break;
            case 'edit':
                $file = 'edit';
                break;
            case 'fields':
                $file = 'fields';
                $replacements = [
                    'fieldform' => $this->genUpdateFields($fields)
                ];
                break;
            case 'index':
                $file = 'index';
                $replacements = [
                    'tablecolumn' => $this->getTableColumn($fields)
                ];
                break;
            case 'show':
                $file = 'show';
                $replacements = [
                    'tablebd' => $this->getFieldShow($fields),
                ];
                break;
            default:
                $file = 'index';
                break;
        }

        $_viewname =  $this->getPluralName();
        if($this->subpath) {
            $_viewname = strtolower($this->subpath).'.'.$_viewname;
        }
        if($this->module) {
            $_viewname = strtolower($this->module->getName()).'::'.$_viewname;
        }

        $replacements = array_merge($replacements, [
            'class'       => $this->getClass(),
            'viewpath'    => $_viewname,
            'routepath'   => $this->getRoutePath(),
            'plural'      => $this->getPluralName(),
            'singular'   => $this->getSingularName(),
            'modelname'   => strtolower($this->getClass()),
            'modulename'  => strtolower($this->module)
        ]);

        $path = config('xgee.stubsOverridePath', __DIR__);

        if (!file_exists($path . "/Stubs/view/{$file}.stub")) {
            $path = __DIR__ . '/../';
        }
        if (!file_exists($path . "/Stubs/view/{$file}.stub")) {
            throw new FileNotFoundException($path . "/Stubs/view/{$file}.stub");
        }

        return Stub::create($path . "/Stubs/view/{$file}.stub", $replacements);
    }

    public function getSingularName()
    {
        return Str::singular(lcfirst(ucwords($this->getClass())));
    }

    /**
     * @return string
     */
    protected function getRoutePath()
    {
        $_module = $this->option('module');
        $_routename = $this->getPluralName();
        if($this->subpath) {
            $_routename = strtolower($this->subpath).'.'.$_routename;
        }
        $_routename = Str::kebab($_routename);
        return $_module?strtolower($_module).'.'.$_routename:$_routename;
    }

    protected function genUpdateFields($fields)
    {
        $_html = '';
        $modelName = lcfirst($this->getName());

        foreach ($fields as $field) {
            if(in_array($field['name'], ['id', 'created_at', 'updated_at', 'create_by', 'deleted_at'])) continue;

            $_html .= '<x-wd-formItem :label="__(\''.$field['name'].'\')" required>'.PHP_EOL;
            $val = '$'.$modelName."->".$field['name']."??''";

            $ruleMap = [
                ['rule' => ['decimal', 'int'], 'content' => '<x-wd-input type="number" name="'.$field['name'].'" :value="'.$val.'" required />'],
                ['rule' => ['timestamp'], 'content' => '<x-wd-input.date type="datetime" name="'.$field['name'].'" :value="'.$val.'" required />'],
                ['rule' => ['text'], 'content' => '<x-wd-input.editor name="'.$field['name'].'" :value="'.$val.'" required />']
            ];

            $_ruleHtml = '';
            foreach($ruleMap as $rule) {
                if(Str::contains($field['type'], $rule['rule'])) {
                    $_ruleHtml = $rule['content'];
                }
            }
            $_html .= $_ruleHtml ? $_ruleHtml : "\t".'<x-wd-input name="'.$field['name'].'" :value="'.$val.'" required />';
            $_html .= PHP_EOL.'</x-wd-formItem>'.PHP_EOL.PHP_EOL;
        }
        return $_html;
    }

    /**
     * @param $fields
     * @param $modelObj
     * @return string
     */
    protected function getFieldShow($fields)
    {
        $modelName = strtolower($this->getName());
        $_html = ''.PHP_EOL;
        foreach ($fields as $field) {
            $_html .= "\t\t\t\t".'<tr><td width=150>{{ __("field.'.$field['name'].'") }}</td><td>{!! $'.$modelName.'->'.$field['name'].' !!}</td></tr>'.PHP_EOL;
        }
        return $_html."\t\t\t";
    }

    /**
     * @param $fields
     * @return string
     */
    protected function getTableHd($fields)
    {
        $_html = ''.PHP_EOL;
        foreach ($fields as $field) {
            $_html .= "\t\t\t\t<th>{{ __('field.{$field['name']}') }}</th>" . PHP_EOL;
        }
        $_html .= "\t\t\t\t<th><i class='fa fa-cogs'></i></th>";
        return $_html.PHP_EOL."\t\t\t";
    }

    /**
     * @param $fields
     * @param $modelObj
     * @return string
     */
    protected function getTableBd($fields)
    {
        $_html = ''.PHP_EOL;
        foreach ($fields as $field) {
            $_html .= "\t\t\t\t".'<td>{{ $item->'.$field['name'].' ?? "" }}</td>'.PHP_EOL;
        }
        $_html .= "\t\t\t\t<td>".PHP_EOL."\t\t\t\t\t".'<form method="post" action="{{ route("'.$this->getRoutePath().'.destroy", $item->id) }}">'.PHP_EOL;
        $_html .= "\t\t\t\t\t\t".'<input type="hidden" name="_method" value="DELETE">'.PHP_EOL;
        $_html .= "\t\t\t\t\t\t".'<input type="hidden" name="_token" value="{{ csrf_token() }}">'.PHP_EOL;

        $_html .= "\t\t\t\t\t\t".'<div class="btn-group">'.PHP_EOL;
        $_html .= "\t\t\t\t\t\t\t".'<a class="btn btn-xs" title="查看" data-toggle="modal" data-position="fixed" href="{{ route("'.$this->getRoutePath().'.show", $item->id) }}"><i class="fa fa-eye"></i></a>'.PHP_EOL;
        $_html .= "\t\t\t\t\t\t\t".'<a class="btn btn-xs" title="编辑" data-toggle="modal" data-position="fixed" href="{{ route("'.$this->getRoutePath().'.edit", $item->id) }}"><i class="fa fa-edit"></i></a>'.PHP_EOL;
        $_html .= "\t\t\t\t\t\t\t".'<button type="submit" class="btn btn-xs btn-danger" onclick="return confirm(\'确认删除?\')"><i class="fa fa-trash "></i></button>'.PHP_EOL;
        $_html .= "\t\t\t\t\t\t".'</div>'.PHP_EOL."\t\t\t\t\t".'</form>'.PHP_EOL."\t\t\t\t".'</td>';
        return $_html.PHP_EOL."\t\t\t";
    }

    protected function getTableColumn($fields)
    {
        $modelName = strtolower($this->getName());
        $_col[] = ['type' => 'checkbox', 'fixed' => 'left'];
        $_hiddenFields = ['created_at'];
        $_titleFields = ['title', 'name'];
        $_fixwidthFields = ['created_at', 'updated_at'];
        foreach ($fields as $field) {
            if($field['name'] == 'id') {
                $_col[] = ['field' => $field['name'], 'title' => '{{__(\''.$field['name'].'\')}}', 'sort' => 'true'];
            }else if(in_array($field['name'], $_hiddenFields)){
                $_col[] = ['field' => $field['name'], 'title' => '{{__(\''.$field['name'].'\')}}', 'hide' =>'true'];
            }else{
                $_col[] = ['field' => $field['name'], 'title' => '{{__(\''.$field['name'].'\')}}'];
            }
        }
        // $_col[] = ['title' =>'操作', 'toolbar' => '#data_'.$modelName.'__tool', 'fixed' => 'right', 'width' => 150];
        return json_encode($_col);
    }
}
