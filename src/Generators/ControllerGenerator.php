<?php
namespace Xbhub\XGee\Generators;

use Xbhub\XGee\Generators\Generator;
use Xbhub\XGee\Generators\RepositoryInterfaceGenerator;
use Xbhub\XGee\Generators\Stub;
use Xbhub\XGee\Generators\ValidatorGenerator;
use Illuminate\Support\Str;

/**
 * Class ControllerGenerator
 * @package Xbhub\XGee\Generators
 */
class ControllerGenerator extends Generator
{
    /**
     * Get stub name.
     *
     * @var string
     */
    protected $stub = 'controller';
    protected $subPath = '';

    public function __construct(array $options = [])
    {
        parent::__construct($options);

        if($_stub = $this->getOption('stub')) {
            $this->stub = $_stub;
        }
        if($_subpath = $this->getOption('subPath')) {
            $this->subPath = $_subpath;
        }
    }

    /**
     * Get generator path config node.
     *
     * @return string
     */
    public function getPathConfigNode()
    {
        return 'controllers';
    }

    /**
     * Get root namespace.
     *
     * @return string
     */
    public function getRootNamespace()
    {
        $_rootNamespace = str_replace('/', '\\', parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode()));
        return $this->subPath?$_rootNamespace.'\\'.$this->subPath:$_rootNamespace;
    }

    public function getSubPath()
    {
        return $this->subPath?strtolower($this->subPath).'.':'';
    }

    /**
     * Get destination path for generated file.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode(), true) . '/' . $this->getControllerName() . 'Controller.php';
    }

        /**
     * Gets controller name based on model
     *
     * @return string
     */
    public function getControllerName()
    {
        return ucfirst($this->getPluralName());
    }

    /**
     * Gets plural name based on model
     *
     * @return string
     */
    public function getPluralName()
    {
        return lcfirst(ucwords($this->getClass()));
    }

    /**
     * Get array replacements.
     *
     * @return array
     */
    public function getReplacements()
    {

        return array_merge(parent::getReplacements(), [
            'controller' => $this->getControllerName(),
            'model'     => $this->getPluralName(),
            'singular'   => $this->getSingularName(),
            'appname'    => $this->getAppNamespace(),
            'subpath'    => $this->getSubPath(),
            'viewpath'   => $this->getViewPath(),
            'modelpath'  => $this->getModelPath(),
            'requestpath'=> $this->getRequestPath(),
            'resourcepath' => $this->getResourcePath()
        ]);
    }

    /**
     * Gets singular name based on model
     *
     * @return string
     */
    public function getSingularName()
    {
        return Str::singular(lcfirst(ucwords($this->getClass())));
    }


    public function getModelPath()
    {
        $modelGenerator = new ModelGenerator([
            'name'      => $this->name,
            'module'    => $this->getOption('module')
        ]);

        $model = $modelGenerator->getRootNamespace() . '\\' . $modelGenerator->getName();

        return 'use ' . str_replace([
                "\\",
                '/'
            ], '\\', $model).';';
    }

    public function getViewPath()
    {
        $_module = $this->getOption('module');
        $_viewPath = $this->getSubPath()?$this->getSubPath().$this->getPluralName():$this->getPluralName();
        return $_module?strtolower($_module).'::'.$_viewPath:$_viewPath;
    }

    public function getRequestPath()
    {
        $modelGenerator = new RequestGenerator([
            'name'      => $this->name,
            'module'    => $this->getOption('module')
        ]);

        $model = $modelGenerator->getRootNamespace() . '\\' . $modelGenerator->getName();

        return 'use ' . str_replace([
                "\\",
                '/'
            ], '\\', $model).'Request;';
    }

    public function getResourcePath()
    {
        $modelGenerator = new ResourceGenerator([
            'name'      => $this->name,
            'module'    => $this->getOption('module')
        ]);

        $model = $modelGenerator->getRootNamespace() . '\\' . $modelGenerator->getName();

        return 'use ' . str_replace([
                "\\",
                '/'
            ], '\\', $model).'Resource;';
    }
}
