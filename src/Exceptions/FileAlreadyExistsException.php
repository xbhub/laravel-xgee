<?php
namespace Xbhub\XGee\Exceptions;

use Exception;

/**
 * Class FileAlreadyExistsException
 * @package Prettus\Repository\Generators
 */
class FileAlreadyExistsException extends Exception
{
}