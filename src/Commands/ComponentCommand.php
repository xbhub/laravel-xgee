<?php
namespace Xbhub\XGee\Commands;

use Illuminate\Console\Command;
use Xbhub\XGee\Generators\ComponentGenerator;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Xbhub\XGee\Exceptions\FileAlreadyExistsException;

class ComponentCommand extends Command
{

    /**
     * The name of command.
     *
     * @var string
     */
    protected $name = 'biu:make-component';

    /**
     * The description of command.
     *
     * @var string
     */
    protected $description = 'create component.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Component';

    /**
     * Execute the command.
     *
     * @see fire()
     * @return void
     */
    public function handle(){
        $this->laravel->call([$this, 'fire'], func_get_args());
    }

    protected $generators = null;

    /**
     * Execute the command.
     *
     * @return void
     */
    public function fire()
    {
        try {
            $this->generators = collect();

            $modelGenerator = new ComponentGenerator([
                'name'     => $this->argument('name'),
                'force'    => $this->option('force'),
                'module'   => $this->option('module')
            ]);
            $this->generators->push($modelGenerator);


            // 执行
            try {
                foreach ($this->generators as $generator) {
                    $generator->run();
                }
            }catch (FileAlreadyExistsException $e) {
                $this->error($e->getMessage().' already exists!');
            }

            $this->info($this->type . ' created successfully.');
        } catch (FileAlreadyExistsException $e) {
            $this->error($this->type . ' already exists!');

            return false;
        }
    }


    /**
     * The array of command arguments.
     *
     * @return array
     */
    public function getArguments()
    {
        return [
            [
                'name',
                InputArgument::REQUIRED,
                'The name of model for which the controller is being generated.',
                null
            ],
        ];
    }


    /**
     * The array of command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return [
            [
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Force the creation if file already exists.',
                null
            ],
            [
                'module',
                null,
                InputOption::VALUE_OPTIONAL,
                'generator module.',
                null,
            ]
        ];
    }
}
