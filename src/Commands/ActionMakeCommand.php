<?php

namespace Xbhub\XGee\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;
use Xbhub\XGee\Generators\Generator;

class ActionMakeCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'biu:make-action {name} {module?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new action';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Action';

    public function handle(){
        (new Generator([
            'name' => $this->argument('name'),
            'module' => $this->argument('module'),
            'stub' => strtolower($this->type),
            'type' => $this->type
        ]))->run();
    }

}
