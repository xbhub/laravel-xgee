<?php

namespace Xbhub\XGee\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class ListenerMakeCommand extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'biu:make-listener';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new event listener class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Listener';

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        $event = $this->option('event');

        if (! Str::startsWith($event, [
            $this->laravel->getNamespace(),
            'Illuminate',
            '\\',
        ])) {
            $event = $this->laravel->getNamespace().'Events\\'.$event;
        }

        $stub = str_replace(
            'DummyEvent', class_basename($event), parent::buildClass($name)
        );

        return str_replace(
            'DummyFullEvent', trim($event, '\\'), $stub
        );
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        if ($this->option('queued')) {
            return $this->option('event')
                        ? __DIR__.'/../Stubs/listener-queued.stub'
                        : __DIR__.'/../Stubs/listener-queued-duck.stub';
        }

        return $this->option('event')
                    ? __DIR__.'/../Stubs/listener.stub'
                    : __DIR__.'/../Stubs/listener-duck.stub';
    }

    /**
     * Determine if the class already exists.
     *
     * @param  string  $rawName
     * @return bool
     */
    protected function alreadyExists($rawName)
    {
        return class_exists($rawName);
    }

    protected function rootNamespace()
    {
        if($module = $this->option('module')) {
            $namespace = $this->laravel['config']->get('modules.namespace');
            return $namespace. DIRECTORY_SEPARATOR .Str::studly($module);
        }
        return $this->laravel->getNamespace();
    }

    public function getDefaultNamespace($rootNamespace) : string
    {
       return $rootNamespace . $this->laravel['modules']->config('paths.listeners', 'Listeners');
    }

    /**
     * stub path
     *
     * @param [type] $name
     * @return void
     */
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        $basePath = $this->option('module')
                        ? module_path($this->option('module'))
                        : $this->laravel['path'];
        return $basePath.'/'.str_replace('\\', '/', $name).'.php';
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['module', null, InputOption::VALUE_OPTIONAL, 'The name of module will be used.'],
            ['event', 'e', InputOption::VALUE_OPTIONAL, 'The event class being listened for'],
            ['queued', null, InputOption::VALUE_NONE, 'Indicates the event listener should be queued'],
        ];
    }
}
