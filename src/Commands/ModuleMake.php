<?php

namespace Xbhub\XGee\Commands;

use Illuminate\Console\GeneratorCommand;

class ModuleMake extends GeneratorCommand
{

    protected function rootNamespace()
    {
        if($module = $this->option('module')) {
            $namespace = $this->laravel['config']->get('modules.namespace');
            return $namespace. DIRECTORY_SEPARATOR .Str::studly($module);
        }
        return $this->laravel->getNamespace();
    }

    /**
     * stub path
     *
     * @param [type] $name
     * @return void
     */
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        $basePath = $this->option('module')
                        ? module_path($this->option('module'))
                        : $this->laravel['path'];
        return $basePath.'/'.str_replace('\\', '/', $name).'.php';
    }
}
