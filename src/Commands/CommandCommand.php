<?php
namespace Xbhub\XGee\Commands;

use Illuminate\Console\Command;
use Xbhub\XGee\Generators\CommandGenerator;
use Xbhub\XGee\Generators\ComponentGenerator;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Xbhub\XGee\Exceptions\FileAlreadyExistsException;

class CommandCommand extends Command
{

    /**
     * The name of command.
     *
     * @var string
     */
    protected $signature = 'biu:make-command {name} {module}';

    /**
     * The description of command.
     *
     * @var string
     */
    protected $description = 'create command.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Command';

    /**
     * Execute the command.
     *
     * @see fire()
     * @return void
     */
    public function handle(){
        $this->laravel->call([$this, 'fire'], func_get_args());
    }

    protected $generators = null;

    /**
     * Execute the command.
     *
     * @return void
     */
    public function fire()
    {
        try {
            $this->generators = collect();

            $modelGenerator = new CommandGenerator([
                'name'     => $this->argument('name'),
                'force'    => $this->option('force'),
                'module'   => $this->argument('module')
            ]);
            $this->generators->push($modelGenerator);


            // 执行
            try {
                foreach ($this->generators as $generator) {
                    $generator->run();
                }
            }catch (FileAlreadyExistsException $e) {
                $this->error($e->getMessage().' already exists!');
            }

            $this->info($this->type . ' created successfully.');
        } catch (FileAlreadyExistsException $e) {
            $this->error($this->type . ' already exists!');

            return false;
        }
    }
}
