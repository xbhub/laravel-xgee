<?php

namespace Xbhub\XGee\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class EventMakeCommand extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'biu:make-event';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new event class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Event';

    /**
     * Determine if the class already exists.
     *
     * @param  string  $rawName
     * @return bool
     */
    protected function alreadyExists($rawName)
    {
        return class_exists($rawName);
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/../Stubs/event.stub';
    }

    public function getDefaultNamespace($rootNamespace) : string
    {
       return $rootNamespace . $this->laravel['modules']->config('paths.events', 'Events');
    }

    protected function rootNamespace()
    {
        if($module = $this->option('module')) {
            $namespace = $this->laravel['config']->get('modules.namespace');
            return $namespace. DIRECTORY_SEPARATOR .Str::studly($module);
        }
        return $this->laravel->getNamespace();
    }

    /**
     * stub path
     *
     * @param [type] $name
     * @return void
     */
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        $basePath = $this->option('module')
                        ? module_path($this->option('module'))
                        : $this->laravel['path'];
        return $basePath.'/'.str_replace('\\', '/', $name).'.php';
    }


    protected function getOptions()
    {
        return [
            ['module', null, InputOption::VALUE_OPTIONAL, 'The name of module will be used.']
        ];
    }

}
