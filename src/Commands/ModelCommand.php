<?php

namespace Xbhub\XGee\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Xbhub\XGee\Exceptions\FileAlreadyExistsException;
use Xbhub\XGee\Generators\MigrationGenerator;
use Xbhub\XGee\Generators\ModelGenerator;
use Xbhub\XGee\Generators\PolicyGenerator;
use Xbhub\XGee\Generators\RepositoryEloquentGenerator;
use Xbhub\XGee\Generators\RepositoryInterfaceGenerator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Str;

class ModelCommand extends Command
{

    /**
     * The name of command.
     *
     * @var string
     */
    protected $name = 'biu:make-model';

    /**
     * The description of command.
     *
     * @var string
     */
    protected $description = 'Create a new model.';

    /**
     * @var Collection
     */
    protected $generators = null;

    protected $type = 'Model';

    /**
     * Execute the command.
     *
     * @see fire()
     * @return void
     */
    public function handle()
    {
        $this->laravel->call([$this, 'fire'], func_get_args());
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function fire()
    {
        $this->generators = new Collection();

        //         生成model
        $modelGenerator = new ModelGenerator([
            'name'     => $this->argument('name'),
            'fillable' => $this->option('fillable'),
            'force'    => $this->option('force'),
            'module' => $this->option('module')
        ]);
        $this->generators->push($modelGenerator);

        // migration
        if (!$this->option('skip-migration')) {
            $migrationGenerator = new MigrationGenerator([
                'name'   => 'create_' . strtolower($this->option('module')) . '_' . Str::snake(Str::plural($this->argument('name'))) . '_table',
                'fields' => $this->option('fillable'),
                'force'  => $this->option('force'),
                'module' => $this->option('module')
            ]);
            $this->generators->push($migrationGenerator);
        }

        // Policy
        $policyGenerator = (new PolicyGenerator([
            'name' => $this->argument('name'),
            'module' => $this->option('module'),
            'force'    => $this->option('force')
        ]));
        $this->generators->push($policyGenerator);


        // 执行
        try {
            foreach ($this->generators as $generator) {
                $generator->run();
            }

            $this->info($this->type . ' created successfully.');
        } catch (FileAlreadyExistsException $e) {
            $this->error($e->getMessage() . ' already exists!');
        }
    }


    /**
     * The array of command arguments.
     *
     * @return array
     */
    public function getArguments()
    {
        return [
            [
                'name',
                InputArgument::REQUIRED,
                'The name of class being generated.',
                null
            ],
        ];
    }


    /**
     * The array of command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return [
            [
                'fillable',
                null,
                InputOption::VALUE_OPTIONAL,
                'The fillable attributes.',
                null
            ],
            [
                'rules',
                null,
                InputOption::VALUE_OPTIONAL,
                'The rules of validation attributes.',
                null
            ],
            [
                'validator',
                null,
                InputOption::VALUE_OPTIONAL,
                'Adds validator reference to the repository.',
                null
            ],
            [
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Force the creation if file already exists.',
                null
            ],
            [
                'skip-migration',
                null,
                InputOption::VALUE_NONE,
                'Skip the creation of a migration file.',
                null,
            ],
            [
                'skip-model',
                null,
                InputOption::VALUE_NONE,
                'Skip the creation of a model.',
                null,
            ],
            [
                'module',
                null,
                InputOption::VALUE_OPTIONAL,
                'generator module.',
                null,
            ]
        ];
    }
}
