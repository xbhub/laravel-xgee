<?php
namespace Xbhub\XGee\Commands;

use File;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Xbhub\XGee\Generators\BindingsGenerator;
use Xbhub\XGee\Exceptions\FileAlreadyExistsException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Xbhub\XGee\Generators\ViewGenerator;

class ViewCommand extends Command
{

    /**
     * The name of command.
     *
     * @var string
     */
    protected $name = 'biu:make-view';

    /**
     * The description of command.
     *
     * @var string
     */
    protected $description = 'create view.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'View';

    /**
     * Execute the command.
     *
     * @see fire()
     * @return void
     */
    public function handle(){
        $this->laravel->call([$this, 'fire'], func_get_args());
    }

    protected $generators = null;

    /**
     * Execute the command.
     *
     * @return void
     */
    public function fire()
    {
        try {
            $this->generators = new Collection();

            $actions = ['create', 'edit', 'fields', 'index'];

            foreach ($actions as $action) {
                $modelGenerator = new ViewGenerator([
                    'name'     => $this->argument('name'),
                    'force'    => $this->option('force'),
                    'module'   => $this->option('module'),
                    'subpath' => $this->option('subpath'),
                    'action'   => $action
                ]);
                $this->generators->push($modelGenerator);
            }


            // 执行
            try {
                foreach ($this->generators as $generator) {
                    $generator->run();
                }
            }catch (FileAlreadyExistsException $e) {
                $this->error($e->getMessage().' already exists!');
            }

            $this->info($this->type . ' created successfully.');
        } catch (FileAlreadyExistsException $e) {
            $this->error($this->type . ' already exists!');

            return false;
        }
    }


    /**
     * The array of command arguments.
     *
     * @return array
     */
    public function getArguments()
    {
        return [
            [
                'name',
                InputArgument::REQUIRED,
                'The name of model for which the controller is being generated.',
                null
            ],
        ];
    }


    /**
     * The array of command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return [
            [
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Force the creation if file already exists.',
                null
            ],
            [
                'subpath',
                null,
                InputOption::VALUE_OPTIONAL,
                'sub path.',
                null
            ],
            [
                'module',
                null,
                InputOption::VALUE_OPTIONAL,
                'generator module.',
                null,
            ]
        ];
    }
}
