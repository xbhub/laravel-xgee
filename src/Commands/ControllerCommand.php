<?php
/**
 * Created by PhpStorm.
 * User: Jory
 * Date: 2018/4/20
 * Time: 上午10:55
 */

namespace Xbhub\XGee\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Xbhub\XGee\Exceptions\FileAlreadyExistsException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Xbhub\XGee\Generators\ControllerGenerator;
use Xbhub\XGee\Generators\PolicyGenerator;
use Xbhub\XGee\Generators\RequestGenerator;
use Xbhub\XGee\Generators\ResourceGenerator;

class ControllerCommand extends Command
{

    /**
     * The signature of command.
     *
     * @var string
     */
    protected $name = 'biu:make-controller';

    /**
     * The description of command.
     *
     * @var string
     */
    protected $description = 'Create a new RESTful controller.';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Controller';

    /**
     * ControllerCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @see fire()
     * @return void
     */
    public function handle(){
        $this->laravel->call([$this, 'fire'], func_get_args());
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function fire()
    {
        try {
            $r = (new ControllerGenerator([
                'name' => $this->argument('name'),
                'force' => $this->option('force'),
                'subPath' => $this->option('subpath'),
                'stub' => $this->option('stub'),
                'module' => $this->option('module')
            ]))->run();

            (new RequestGenerator([
                'name' => $this->argument('name'),
                'force' => $this->option('force'),
                'module' => $this->option('module')
            ]))->run();

            (new ResourceGenerator([
                'name' => $this->argument('name'),
                'force' => $this->option('force'),
                'module' => $this->option('module')
            ]))->run();

            $this->info($this->type . ' created successfully.');

        } catch (FileAlreadyExistsException $e) {
            $this->error($this->type . ' already exists!');

            return false;
        }
    }


    /**
     * The array of command arguments.
     *
     * @return array
     */
    public function getArguments()
    {
        return [
            [
                'name',
                InputArgument::REQUIRED,
                'The name of model for which the controller is being generated.',
                null
            ],
        ];
    }


    /**
     * The array of command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return [
            [
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Force the creation if file already exists.',
                null
            ],
            [
                'subpath',
                null,
                InputOption::VALUE_OPTIONAL,
                '子路径.',
                null
            ],
            [
                'stub',
                null,
                InputOption::VALUE_OPTIONAL,
                '自定义模板.',
                null
            ],
            [
                'module',
                null,
                InputOption::VALUE_OPTIONAL,
                'generator module.',
                null,
            ]
        ];
    }
}
