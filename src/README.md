# laravel-xgee

专为lumina特制的代码生成器

```php
          

biu:controller                Create a new RESTful controller.
biu:criteria                  Create a new criteria.
biu:model                     Create a new model.
biu:policy                    Create a new policy class
biu:repository                Create a new repository.
biu:request                   Create a new form request class
biu:view                      create view.


```

## How to use

1. `biu:model Post --module=Demo`, 并配置好migration和对应model的fillable字段
2. `biu:controller Post --module=Demo`, 生成控制器
3. `biu:view Post --module=Demo`, 生成视图
4. 自行配置路由`Route::resource('post', 'PostController')`

Enjoy!


## License

MIT
