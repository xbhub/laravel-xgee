<?php
namespace Xbhub\XGee;

use Illuminate\Support\ServiceProvider;

/**
 * Class RepositoryServiceProvider
 */
class XGeeServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;


    /**
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/xgee.php' => config_path('xgee.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../config/xgee.php', 'xgee'
        );
    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands('Xbhub\XGee\Commands\ModelCommand');
        $this->commands('Xbhub\XGee\Commands\ControllerCommand');
        $this->commands('Xbhub\XGee\Commands\RequestCommand');
        $this->commands('Xbhub\XGee\Commands\PolicyCommand');
        $this->commands('Xbhub\XGee\Commands\ViewCommand');
        $this->commands('Xbhub\XGee\Commands\ComponentCommand');
        $this->commands('Xbhub\XGee\Commands\ListenerMakeCommand');
        $this->commands('Xbhub\XGee\Commands\EventMakeCommand');
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
