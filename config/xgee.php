<?php

return [
    'basePath'      => app()->path(),
    'stubsOverridePath' => app()->path(),
    'paths'         => [
        'models'       => 'Models',
        'controllers'  => 'Http/Controllers',
        'requests'     => 'Http/Requests',
        'policies'     => 'Http/Policies',
        'resource'     => 'Http/Resources',
        'component'    => 'View/Components',
        'listeners'    => 'Listeners',
        'events'       => 'Events'
    ]
];

